function counter_factory() {
    let counter = 0;
    return {
        inc: function increment() {
            counter++;
            return counter;
        },
        dec: function decrement() {
            counter--;
            return counter;
        }
    };
}

export { counter_factory };