import { test_call_count } from "./test/limit_function_call_count_test.js";
import { cache_function_test } from "./test/cache_function_test.js"
import { counter_factory_test } from "./test/counter_factory_test.js"

console.log("value used is 5 a line should be printed exactly 5 times");
test_call_count();
console.log("\n");
console.log("increment and decrement are called a few times");
counter_factory_test();
console.log("\n");
console.log("Each value of the array would be called exactly once")
cache_function_test();