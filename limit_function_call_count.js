function limit_function_call_count(cb, n) {
    let counter = 1;
    function my_func() {
        if (counter <= n) {
            cb(counter);
            counter++;

        }
    }
    return my_func;
}
export { limit_function_call_count };