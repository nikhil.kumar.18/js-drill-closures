import { counter_factory } from "../counter_factory.js";
function counter_factory_test() {
    const temp_func = counter_factory();
    console.log(temp_func.inc());
    console.log(temp_func.inc());
    console.log(temp_func.inc());
    console.log(temp_func.dec());
    console.log(temp_func.inc());
    console.log(temp_func.dec());
    console.log(temp_func.dec());
}

export { counter_factory_test };