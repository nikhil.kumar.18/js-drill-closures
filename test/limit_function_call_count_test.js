import { limit_function_call_count } from '../limit_function_call_count.js';
import { print } from '../extra_var_func.js';
function test_call_count() {
    let val = 5;
    const temp_func = limit_function_call_count(print, val);
    //now we will call temp_func 10 times but print will be invoked 5 times only
    for (let iter = 1; iter <= 10; iter++) {
        temp_func();
    }
}
export { test_call_count };