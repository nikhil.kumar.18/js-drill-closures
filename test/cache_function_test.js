import { cache_function } from '../cache_function.js';
import { mul_by_2 } from "../extra_var_func.js";

function cache_function_test() {
    const arr = [1, 2, 3, 2, 3, 1];
    const temp_func = cache_function(mul_by_2);
    //function mul_by_2 would be called exactly once for each element in the arr
    for (let val in arr) {
        console.log(temp_func(arr[val]));
    }
}
export { cache_function_test };
