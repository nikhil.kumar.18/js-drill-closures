function print(n) {
    console.log(`printing ${n} times`);
}

function mul_by_2(val) {
    return val * 2;
}
export { print, mul_by_2 };