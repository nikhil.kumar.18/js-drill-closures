function cache_function(cb) {
    const obj = {};
    function my_func(arg) {
        if (obj[arg] === undefined) {
            obj[arg] = cb(arg);

        }

        return obj[arg];
    }
    return my_func;
}

export { cache_function };
